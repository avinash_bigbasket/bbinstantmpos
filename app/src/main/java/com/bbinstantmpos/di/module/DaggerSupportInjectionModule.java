package com.bbinstantmpos.di.module;

import java.util.Map;

import androidx.fragment.app.Fragment;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.internal.Beta;
import dagger.multibindings.Multibinds;

@Beta
@Module(includes = DaggerInjectionModule.class)
public abstract class DaggerSupportInjectionModule {

    @Multibinds
    abstract Map<Class<? extends Fragment>, AndroidInjector.Factory<? extends Fragment>>
    supportFragmentInjectorFactories();

    private DaggerSupportInjectionModule() {
    }
}
