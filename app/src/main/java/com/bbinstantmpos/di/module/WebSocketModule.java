package com.bbinstantmpos.di.module;

import android.content.Context;

import com.bbinstantmpos.services.WebSocketCommunicator;
import com.bbinstantmpos.services.WebSocketMessageCreator;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module(includes = MessageConstructorModule.class)
public class WebSocketModule {

    @Provides
    WebSocketCommunicator providesWebSocketCommunicator(Context context, OkHttpClient okHttpClient,
                                                        WebSocketMessageCreator webSocketMessageCreator) {

        return new WebSocketCommunicator(context, okHttpClient, webSocketMessageCreator);
    }

}
