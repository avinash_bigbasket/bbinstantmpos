package com.bbinstantmpos.di.module;

import android.content.Context;

import com.bbinstantmpos.ui.machieview.MachineViewModel;
import com.bbinstantmpos.util.DeviceUtils;
import com.bbinstantmpos.util.DialogUtils;

import dagger.Module;
import dagger.Provides;

@Module
public class UtilsModule {

    @Provides
    DeviceUtils provideDeviceUtils(Context context) {

        return new DeviceUtils(context);
    }

    /*@Provides
    DialogUtils providesDialogUtils(Context context) {

        return new DialogUtils(context);
    }*/

}
