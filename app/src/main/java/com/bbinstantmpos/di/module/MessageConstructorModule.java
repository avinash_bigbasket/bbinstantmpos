package com.bbinstantmpos.di.module;

import com.bbinstantmpos.services.WebSocketMessageCreator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MessageConstructorModule {

    @Singleton
    @Provides
    WebSocketMessageCreator providesMsgCreator() {
        return new WebSocketMessageCreator();
    }
}
