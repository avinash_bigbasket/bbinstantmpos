package com.bbinstantmpos.di.module;

import com.bbinstantmpos.ui.machieview.MachineViewActivity;
import com.bbinstantmpos.ui.machinelist.MachineListActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    abstract MachineViewActivity bindMachineViewActivity();

    @ContributesAndroidInjector(modules = MachineListBindingModule.class)
    abstract MachineListActivity bindListActivity();

}