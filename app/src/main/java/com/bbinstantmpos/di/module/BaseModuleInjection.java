package com.bbinstantmpos.di.module;

import android.app.Activity;

import java.util.Map;

import androidx.lifecycle.AndroidViewModel;
import androidx.recyclerview.widget.RecyclerView;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.multibindings.Multibinds;
import io.reactivex.annotations.Beta;

@Beta
@Module
public abstract class BaseModuleInjection {

    @Multibinds
    abstract Map<Class<? extends BaseModule>, AndroidInjector.Factory<? extends BaseModule>>
    baseModuleInjectorFactories();

    @Multibinds
    abstract Map<Class<? extends AndroidViewModel>, AndroidInjector.Factory<? extends AndroidViewModel>>
    viewModelInjectorFactories();

    @Multibinds
    abstract Map<Class<? extends AndroidViewModel>,
    AndroidInjector.Factory<? extends RecyclerView.Adapter>>
    adapterInjectorFactories();

    private BaseModuleInjection() {}
}
