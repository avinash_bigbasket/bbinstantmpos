package com.bbinstantmpos.di.module;

import android.content.Context;

import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;

@Module
public class PicassoModule {

    @Provides
    public Picasso providePicasso(Context context) {

        return new Picasso.Builder(context)
                .loggingEnabled(true)
                .build();
    }
}
