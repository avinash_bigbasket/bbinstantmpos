package com.bbinstantmpos.di.module;

import android.app.Application;
import android.content.Context;

import com.bbinstantmpos.data.source.AthenticationIntercepter;
import com.bbinstantmpos.data.source.InternetAvailabilityIntercepter;
import com.bbinstantmpos.data.source.TokenAuthenticator;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
public class OkHttpClientModule {

    @Provides
    public OkHttpClient okHttpClient(Context context, InternetAvailabilityIntercepter internetAvailabilityIntercepter, HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient()
                .newBuilder()
                .authenticator(new TokenAuthenticator(context))
                .addInterceptor(internetAvailabilityIntercepter)
                .addInterceptor(new AthenticationIntercepter(context))
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    public InternetAvailabilityIntercepter internetAvailabilityIntercepter() {
        return new InternetAvailabilityIntercepter();
    }

    @Provides
    public HttpLoggingInterceptor providesLoggingIntercepter(){
        HttpLoggingInterceptor  interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

}