package com.bbinstantmpos.di.module;

import android.util.Log;

import com.bbinstantmpos.data.source.BBInstantService;
import com.bbinstantmpos.data.source.InternetAvailabilityIntercepter;
import com.bbinstantmpos.util.UrlConstants;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = OkHttpClientModule.class)
public class ApplicationModule {

    @Singleton
    @Provides
    static Retrofit provideRetrofit(OkHttpClient okHttpClient) {

        return new Retrofit.Builder()
                .baseUrl(UrlConstants.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .build();
    }

    @Singleton
    @Provides
    static BBInstantService provideRetrofitService(Retrofit retrofit) {
        return retrofit.create(BBInstantService.class);
    }


}
