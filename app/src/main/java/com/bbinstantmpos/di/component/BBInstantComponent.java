package com.bbinstantmpos.di.component;

import android.app.Application;

import com.bbinstantmpos.di.module.ActivityBindingModule;
import com.bbinstantmpos.di.module.ApplicationModule;
import com.bbinstantmpos.di.module.ContextModule;
//import com.bbinstantmpos.di.module.DaggerSupportInjectionModule;
import com.bbinstantmpos.di.module.DaggerSupportInjectionModule;
import com.bbinstantmpos.di.module.PicassoModule;
import com.bbinstantmpos.di.module.UtilsModule;
import com.bbinstantmpos.di.module.WebSocketModule;
import com.bbinstantmpos.ui.base.BBInstantApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

@Singleton
@Component(modules = {UtilsModule.class, WebSocketModule.class, PicassoModule.class, ContextModule.class,
        ApplicationModule.class, DaggerSupportInjectionModule.class,
        ActivityBindingModule.class})
public interface BBInstantComponent extends AndroidInjector<DaggerApplication> {

    void inject(BBInstantApplication bbInstantApplication);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder applicaton(Application application);

        BBInstantComponent build();
    }
}
