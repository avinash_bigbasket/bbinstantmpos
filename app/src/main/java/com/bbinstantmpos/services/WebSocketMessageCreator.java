package com.bbinstantmpos.services;

import android.content.Context;
import android.text.TextUtils;

import com.bbinstantmpos.entities.MachineTray;
import com.bbinstantmpos.entities.TrayDetails;

import okhttp3.OkHttpClient;

public class WebSocketMessageCreator {


    public String getMsgInFormat(String command, String clientId) {
        try {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append(clientId);

            stringBuilder.append(":");
            stringBuilder.append(command);
//            if (!TextUtils.isEmpty(orderId)) {
//                stringBuilder.append(":");
//                stringBuilder.append(orderId);
//            }
            return stringBuilder.toString();
        } catch (Exception e) {

            return null;
        }
    }

    public String prepareDoorOpenMsg(MachineTray container, String clientId){

        long timestamp = System.currentTimeMillis() + 1000;
        StringBuilder builder = new StringBuilder();
        builder.append(clientId);
        builder.append(":");
        builder.append(container.getLockCode());
        builder.append(":");
        builder.append(container.getTrayName());
        builder.append(":");
        builder.append(MessengerConstants.REQUEST_TO_DOOR_OPEN);
        builder.append(":");
        builder.append(timestamp);
        return builder.toString();
    }

    public String parseWebSocketMessage(String text) {

        String[] msgParts = text.split(":");
        if (msgParts.length<2) throw new IllegalStateException("invalid msg type");
//        if (isOffline){
//            msg.machineId = msgParts[0];
//        }else {
//            msg.clientId = msgParts[0];
//        }
//        msg.messengerCommands = msgParts[1];
        return msgParts[1];
    }
}
