package com.bbinstantmpos.services;

public interface MessengerConstants {
    String MACHINE_BUSY = "busy";
    String MACHINE_FREE = "free";
    String MACHINE_CONNECT_NEW = "connectnew";
    String MACHINE_CLIENT = "client";
    String MACHINE_DOOR_CLOSED = "doorclosed";
    String MACHINE_DOOR_OPEN = "dooropened";
    String MACHINE_TIMEOUT = "timeout";
    String MACHINE_TIME_EXPIRED = "timeexpired";
    String MACHINE_DISCONNECT = "disconnect";
    String MACHINE_CLOUD = "cloud";
    String MACHINE_OFFLINE = "Machine Not available";
    String MACHINE_ONLINE = "Machine Available";
    String REQUEST_TO_DOOR_OPEN = "open";
}
