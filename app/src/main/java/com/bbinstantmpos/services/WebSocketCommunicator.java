package com.bbinstantmpos.services;

import android.content.Context;

import com.bbinstantmpos.R;
import com.bbinstantmpos.entities.MachineTray;
import com.bbinstantmpos.exceptions.MessageNotSentException;
import com.bbinstantmpos.exceptions.SocketDisconnectedException;

import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import timber.log.Timber;

public class WebSocketCommunicator extends WebSocketListener {

    private Subject<Boolean> isSocketConnected = PublishSubject.create();

    private Subject<String> messageCommands = PublishSubject.create();

    private String ipAddress = "192.168.8.100";

    private String offlinePort = "9090";

    public String clientId = "ijpd8sq73e1671ako3lpgleknffovhsf";

    private String offlineIp;

    private Context context;

    private OkHttpClient okHttpClient;
    private WebSocket webSocket;

    private boolean isMachineConnected;

    WebSocketMessageCreator webSocketMessageCreator;

    private MutableLiveData<String> webSocketResponse = new MutableLiveData<>();

    public WebSocketCommunicator(Context context, OkHttpClient okHttpClient, WebSocketMessageCreator webSocketMessageCreator) {

        this.context = context;
        this.okHttpClient = okHttpClient;
        this.webSocketMessageCreator = webSocketMessageCreator;
        offlineIp = String.format(context.getResources().getString(R.string.local_ws_url), ipAddress, offlinePort,
                clientId);

        isSocketConnected.subscribe(aBoolean -> isMachineConnected = aBoolean);

        Timber.plant(new Timber.DebugTree());
    }

    public Observable<Boolean> connectToMachine() {
        return Observable.fromCallable(() -> {
            createWebSocket();
            return 1;
        }).flatMap(integer -> isSocketConnected);
    }

    public void createWebSocket() {
        Timber.d("creating web socket: " + context.getResources().getString(R.string.local_ws_url));
        Request request = prepareRequest();
        webSocket = okHttpClient.newWebSocket(request, this);
    }

    private Request prepareRequest() {
        return new Request.Builder()
                .url(offlineIp)
                .build();
    }

    public Observable<Boolean> openDoor(MachineTray machineTray) {

        return Observable.fromCallable(() -> {

            if (webSocket == null)
                throw new SocketDisconnectedException();
            return webSocket.send(webSocketMessageCreator.prepareDoorOpenMsg(machineTray, clientId));
        }).flatMap(aBoolean -> {
            if (aBoolean) {
                Timber.d("door open----------> message sent to machine");
                return messageCommands.take(2);
            } else {

                Timber.d("door open----------> message sending failed");
                throw new MessageNotSentException();
            }
        })
                .map(msg -> {
                    if (msg.equals(MessengerConstants.MACHINE_DOOR_OPEN)) {

                        return true;
                    }
                    return false;
                });
    }

    public Subject<String> subscribeToWebSocketMessages() {

        return messageCommands;
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        super.onOpen(webSocket, response);
        webSocket.send(webSocketMessageCreator
                .getMsgInFormat(MessengerConstants.MACHINE_CONNECT_NEW, null));
        isSocketConnected.onNext(true);
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        super.onMessage(webSocket, text);

        messageCommands.onNext(webSocketMessageCreator.parseWebSocketMessage(text));
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason) {
        super.onClosed(webSocket, code, reason);
        isSocketConnected.onNext(false);
        messageCommands.onComplete();
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        super.onClosing(webSocket, code, reason);

        isSocketConnected.onNext(false);
        messageCommands.onComplete();
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, @Nullable Response response) {
        super.onFailure(webSocket, t, response);
        isSocketConnected.onNext(false);
        messageCommands.onComplete();
    }

}
