//package com.bbinstantmpos.ui.adapter;
//
//import android.content.Context;
//import android.graphics.Rect;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.bbinstantmpos.R;
//import com.bbinstantmpos.entities.MachineTray;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//import java.util.Map;
//import java.util.TreeMap;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//public class MachineViewAdapter extends RecyclerView.Adapter<MachineViewAdapter.ViewHolder> {
//
//    private MachineViewProductAdapter machineViewRowAdapter;
//    private List<List<MachineTray>> trayContentList;
//    private int maxTrayCount;
//    private Context mContext;
//
//    public MachineViewAdapter(Context context, List<MachineTray> trays) {
//
//        mContext = context;
//        trayContentList = getTraysSorted(trays);
//        maxTrayCount = getMaxTrayCount();
//    }
//
//    @NonNull
//    @Override
//    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//
//        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//        View view = inflater.inflate(R.layout.machine_view_row, parent, false);
//        return new ViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//
//        holder.bindData(trayContentList.get(position));
//    }
//
//    @Override
//    public int getItemCount() {
//        return trayContentList.size();
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//
//        private TextView container_name;
//        private RecyclerView recyclerView;
//
//        public ViewHolder(@NonNull View itemView) {
//            super(itemView);
//
//            container_name = itemView.findViewById(R.id.container_name);
//            recyclerView = itemView.findViewById(R.id.recycler_machine_row);
//
//            recyclerView.setHasFixedSize(true);
//            recyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
//            //recyclerView.addItemDecoration(new HorizontalSpaceItemDecoration());
//            recyclerView.setNestedScrollingEnabled(false);
//
//            machineViewRowAdapter = new MachineViewProductAdapter(mContext, maxTrayCount);
//            recyclerView.setAdapter(machineViewRowAdapter);
//        }
//
//        private void bindData(List<MachineTray> trayList) {
//
//            machineViewRowAdapter.bindAdapter(trayList);
//        }
//    }
//
//    private List<List<MachineTray>> getTraysSorted(List<MachineTray> machineTrays) {
//
//        Map<String, List<MachineTray>> trayMap = new TreeMap<>();
//        for (MachineTray tray: machineTrays){
//            String containerName = tray.getTrayName().substring(0,1);
//            if (!trayMap.containsKey(containerName)){
//                List<MachineTray> trays = new ArrayList<>();
//                trays.add(tray);
//                trayMap.put(containerName, trays);
//            }else {
//                trayMap.get(containerName).add(tray);
//            }
//        }
//
//        List<List<MachineTray>> sortedMachineTrays = new ArrayList<>();
//        for (Map.Entry<String, List<MachineTray>> entry: trayMap.entrySet()){
//            Collections.sort(entry.getValue(), new Comparator<MachineTray>() {
//                @Override
//                public int compare(MachineTray o1, MachineTray o2) {
//                    return o1.getTrayName().compareTo(o2.getTrayName());
//                }
//            });
//            sortedMachineTrays.add(entry.getValue());
//        }
//
//        return sortedMachineTrays;
//    }
//
//    private int getMaxTrayCount() {
//
//        for (List<MachineTray> trayList : trayContentList) {
//            if (trayList.size() > maxTrayCount) {
//
//                maxTrayCount = trayList.size();
//            }
//        }
//
//        return maxTrayCount;
//    }
//
//    public class HorizontalSpaceItemDecoration extends RecyclerView.ItemDecoration {
//
//        private final int HorizontalSpaceHeight = 15;
//
//        @Override
//        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//            outRect.right = HorizontalSpaceHeight;
//        }
//    }
//}
