package com.bbinstantmpos.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bbinstantmpos.R;
import com.bbinstantmpos.entities.Machines;
import com.bbinstantmpos.util.SingleLiveEvent;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MachineListAdapter extends RecyclerView.Adapter<MachineListAdapter.ViewHolder> {

    private List<Machines> machinesList;

    private SingleLiveEvent<Machines> machineSelectedLiveEvent = new SingleLiveEvent<>();

    public MachineListAdapter(Context context, List<Machines> machinesList) {

        this.machinesList = machinesList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.machine_list_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.bindData(machinesList.get(position), position);

    }

    @Override
    public int getItemCount() {
        return machinesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView locationName, machineName;
        View view;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            view = itemView;
            machineName = itemView.findViewById(R.id.machine_name);
            locationName = itemView.findViewById(R.id.location_name);
        }

        public void bindData(Machines machines, int position) {

            if (position == 0) {
                locationName.setText(machines.getLocationName());
                locationName.setVisibility(View.VISIBLE);

            } else locationName.setVisibility(View.GONE);

            if (position > 0 && !machinesList.get(position - 1).getLocationName()
                    .equals(machines.getLocationName())) {
                locationName.setText(machines.getLocationName());
                locationName.setVisibility(View.VISIBLE);
            } else if (position != 0){

                locationName.setVisibility(View.GONE);
            }

            machineName.setText(machines.getDisplayName());

            view.setOnClickListener(v -> machineSelectedLiveEvent.setValue(machines));
        }
    }

    public SingleLiveEvent<Machines> observeAdapterData() {

        return machineSelectedLiveEvent;
    }
}
