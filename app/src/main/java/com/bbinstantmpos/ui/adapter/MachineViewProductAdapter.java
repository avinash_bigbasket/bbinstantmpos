package com.bbinstantmpos.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bbinstantmpos.R;
import com.bbinstantmpos.entities.MachineTray;
import com.bbinstantmpos.util.DeviceUtils;
import com.bbinstantmpos.util.SingleLiveEvent;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import timber.log.Timber;

public class MachineViewProductAdapter extends RecyclerView.Adapter<MachineViewProductAdapter.ViewHolder> {

    private SingleLiveEvent<MachineTray> machineTraySingleLiveEvent = new SingleLiveEvent<>();

    @Inject
    DeviceUtils deviceUtils;

    private int maxTrayCount;
    private List<MachineTray> mTrayList;
    private Resources resources;
    private Context mContext;

    private boolean isWebSocketConnected;


    public MachineViewProductAdapter(Context context, List<MachineTray> trayList, int maxTrayCount, boolean isWebSocketConnected) {

        this.maxTrayCount = maxTrayCount;
        this.mTrayList = trayList;
        resources = context.getResources();

        mContext = context;

        deviceUtils = new DeviceUtils(context);

        this.isWebSocketConnected = isWebSocketConnected;

        Timber.plant(new Timber.DebugTree());

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.machine_product_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (position >= mTrayList.size()) {
            holder.bindData(mTrayList.get(0));
            holder.hideView();
        } else
            holder.bindData(mTrayList.get(position));
    }


    @Override
    public int getItemCount() {
        return maxTrayCount;
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        ImageView product_image;
        TextView product_name;
        Button btn_buy;
        LinearLayout layoutProduct;
        CardView card_view_products;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            product_image = itemView.findViewById(R.id.product_image);
            product_name = itemView.findViewById(R.id.product_name);
            btn_buy = itemView.findViewById(R.id.buy);
            layoutProduct = itemView.findViewById(R.id.layout_product);
            card_view_products = itemView.findViewById(R.id.card_view_products);

            setupViewHeight(itemView);
            setImageViewHeight(product_image);
        }

        private void bindData(MachineTray machineTray) {

            card_view_products.setVisibility(View.VISIBLE);
            product_name.setText(machineTray.getMachineProduct().getName());
            Picasso.get().load(machineTray.getMachineProduct().getImage()).into(product_image);

            if (isWebSocketConnected)
                btn_buy.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
            else
                btn_buy.setBackgroundColor(mContext.getResources().getColor(R.color.color_disabled));

            btn_buy.setOnClickListener(v -> machineTraySingleLiveEvent.setValue(machineTray));

        }

        void hideView() {

            card_view_products.setVisibility(View.INVISIBLE);
        }

        private void setupViewHeight(View view) {
            int height = deviceUtils.getHeightOfScreen() / 4 + 100;
            int width = deviceUtils.getWidthOfScreen() / 4 + 100;

            ViewGroup.LayoutParams layoutParams = layoutProduct.getLayoutParams();
            layoutParams.height = height;
            layoutParams.width = width;
            layoutProduct.setLayoutParams(layoutParams);
        }

        private void setImageViewHeight(ImageView imageView) {
            int height = (int) (deviceUtils.getHeightOfScreen() / 4);
            int width = deviceUtils.getWidthOfScreen() / 4;

            ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
            layoutParams.height = height;
            layoutParams.width = width;
            imageView.setLayoutParams(layoutParams);
        }

    }

    public SingleLiveEvent<MachineTray> getMachineTraySingleLiveEvent() {

        return machineTraySingleLiveEvent;
    }
}
