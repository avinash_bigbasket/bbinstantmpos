package com.bbinstantmpos.ui.machinelist;

import android.app.Application;

import com.bbinstantmpos.data.source.BBInstantRepository;
import com.bbinstantmpos.entities.Locations;
import com.bbinstantmpos.entities.Machines;
import com.bbinstantmpos.ui.machieview.MachineViewStates;
import com.bbinstantmpos.util.Preferences;
import com.bbinstantmpos.util.SingleLiveEvent;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class MachineListViewModel extends AndroidViewModel {

    private BBInstantRepository bbInstantRepository;

    private MutableLiveData<List<Locations>> locationsMutableLiveData = new MutableLiveData<>();

    private MutableLiveData<String> onMessageReceived = new SingleLiveEvent<>();

    private CompositeDisposable compositeDisposable;

    private Application mApplication;

    public MachineListViewModel(@NonNull Application application, BBInstantRepository bbInstantRepository) {
        super(application);

        this.bbInstantRepository = bbInstantRepository;

        compositeDisposable = new CompositeDisposable();

        mApplication = application;
    }

    public void initialise() {

        Disposable d = bbInstantRepository.getLocationList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(locations -> locationsMutableLiveData.setValue(locations),
                        throwable -> onMessageReceived.setValue(MachineViewStates.STATE_ERROR_RECEIVED));

        compositeDisposable.add(d);
    }

    public List<Machines> getMachineList(List<Locations> locationsList) {

        List<Machines> machinesList = new ArrayList<>();
        for (Locations location : locationsList) {

            for (int i = 0; i < location.getMachines().size(); i++) {

                location.getMachines().get(i).setLocationName(location.getLocationName());
                machinesList.add(location.getMachines().get(i));

            }
        }

        return machinesList;
    }

    public MutableLiveData<List<Locations>> getLocations() {

        return locationsMutableLiveData;
    }

    public MutableLiveData<String> getOnMessageReceived() {

        return onMessageReceived;
    }

    public void setReferences(Machines machines) {

        Preferences.setPreferences(mApplication.getBaseContext(), Preferences.OFFLINE_IP,
                machines.getOfflineIpAddress());

        Preferences.setPreferences(mApplication.getBaseContext(), Preferences.OFFLINE_PORT,
                machines.getOfflinePort());

        Preferences.setPreferences(mApplication.getBaseContext(), Preferences.MACHINE_ID,
                machines.getMachineId());
    }

}
