package com.bbinstantmpos.ui.machinelist;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.bbinstantmpos.R;
import com.bbinstantmpos.entities.Machines;
import com.bbinstantmpos.ui.adapter.MachineListAdapter;
import com.bbinstantmpos.ui.machieview.MachineViewStates;
import com.bbinstantmpos.util.Preferences;
import com.bbinstantmpos.util.ViewModelFactory;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import dagger.android.support.DaggerAppCompatActivity;

public class MachineListActivity extends DaggerAppCompatActivity {

    private MachineListViewModel machineListViewModel;

    private RecyclerView recyclerView;

    private SwipeRefreshLayout swipeRefreshLayout;

    @Inject
    ViewModelFactory factory;

    private MachineListAdapter machinesListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container_layout);

//        initUI();
//
//        setUpActionbar();
//
//        initialise();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, MachineListFragment.newInstance())
                    .commitNow();
        }
    }

    private void initUI() {

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_machines);
        recyclerView = findViewById(R.id.recycler_machine_list);

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                swipeRefreshLayout.setRefreshing(true);
                machineListViewModel.initialise();
            }
        });
    }

    private void setUpActionbar() {

        getSupportActionBar().setTitle("Select Machines");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;

    }

    private void initialise() {

        machineListViewModel = ViewModelProviders.of(this, factory).get(MachineListViewModel.class);

        swipeRefreshLayout.setRefreshing(true);
        machineListViewModel.initialise();

        observerServerRequest();

        observerRecievedMessage();
    }

    private void observerRecievedMessage() {
        machineListViewModel.getOnMessageReceived().observe(this, s -> {

            switch (s) {
                case MachineViewStates.STATE_ERROR_RECEIVED:

                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(this, "Data Loading Failed", Toast.LENGTH_SHORT).show();
                    break;
            }
        });
    }

    private void observerServerRequest() {

        machineListViewModel.getLocations().observe(this, locationsList -> {

            swipeRefreshLayout.setRefreshing(false);

            List<Machines> machinesList = machineListViewModel.getMachineList(locationsList);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);

            machinesListAdapter = new MachineListAdapter(this, machinesList);
            recyclerView.setAdapter(machinesListAdapter);

            observeAdapterDataChange();
        });
    }

    private void observeAdapterDataChange() {

        machinesListAdapter.observeAdapterData().observeSingle(this, machines -> {

            machineListViewModel.setReferences(machines);

            Intent intent = new Intent();
            intent.putExtra(MachineViewStates.STATE_MACHINE_LIST_SELECTED, true);
            setResult(RESULT_OK, intent);
            finish();
        });
    }


}
