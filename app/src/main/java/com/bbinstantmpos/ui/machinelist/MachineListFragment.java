package com.bbinstantmpos.ui.machinelist;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bbinstantmpos.R;
import com.bbinstantmpos.util.AndroidSupportInjection;
import com.bbinstantmpos.util.ViewModelFactory;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import dagger.android.support.DaggerFragment;


public class MachineListFragment extends DaggerFragment {

    private MachineListViewModel mViewModel;

    @Inject
    ViewModelFactory factory;

    public static MachineListFragment newInstance() {
        return new MachineListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.machine_list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        mViewModel = ViewModelProviders.of(this, factory).get(MachineListViewModel.class);

    }

    @Override
    public void onAttach(Context context) {

//        AndroidSupportInjection.inject(this);

        AndroidSupportInjection.inject(this);

        super.onAttach(context);
    }
}
