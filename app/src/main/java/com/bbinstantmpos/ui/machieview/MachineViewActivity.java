package com.bbinstantmpos.ui.machieview;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bbinstantmpos.R;
import com.bbinstantmpos.entities.MachineTray;
import com.bbinstantmpos.ui.adapter.MachineViewProductAdapter;
import com.bbinstantmpos.ui.machinelist.MachineListActivity;
import com.bbinstantmpos.util.DialogUtils;
import com.bbinstantmpos.util.ViewModelFactory;
import com.eze.api.EzeAPI;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class MachineViewActivity extends DaggerAppCompatActivity {

    private static final int STATE_MACHINE_LIST_SELECTED = 101;
    private MachineViewModel machineViewModel;
    private List<List<MachineTray>> trayContentList;
    private LinearLayout mContainer;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Inject
    ViewModelFactory factory;

    @Inject
    DialogUtils dialogUtils;

    private int maxTrayCount;
    private boolean isScrolling;

    private boolean isWebSocketConnected;
    private String strTxnId;

    private MachineTray machineTrayValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
        initialize();
    }

    private void initUI() {

        swipeRefreshLayout = findViewById(R.id.swiperefresh_products);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mContainer = findViewById(R.id.container);
    }

    private void initialize() {

        machineViewModel = ViewModelProviders.of(this, factory).get(MachineViewModel.class);
        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setRefreshing(true);

        machineViewModel.initialise();

        observeDataChange();

        observetMessageRecieved();
    }

    private void observetMessageRecieved() {

        machineViewModel.getOnMessageReceived().observeSingle(this, messageObject -> {

            switch (messageObject.message) {

                case MachineViewStates.STATE_WEB_SOCKET_CONNECTION_SUCCESSFUL:

                    isWebSocketConnected = true;
                    if (trayContentList != null && trayContentList.size() > 0) {
                        mContainer.removeAllViews();
                        setUpRecyclerView();
                    }
                    Toast.makeText(this, "Connection successful", Toast.LENGTH_SHORT).show();
                    break;

                case MachineViewStates.STATE_DOOR_OPENED:

                    Toast.makeText(this, "Door Opened", Toast.LENGTH_SHORT).show();
                    break;

                case MachineViewStates.STATE_WEB_SOCKET_CONNECTION_FAILED:

                    isWebSocketConnected = false;
                    Toast.makeText(this, "Connection failed", Toast.LENGTH_SHORT).show();
                    break;
                case MachineViewStates.STATE_ERROR_RECEIVED:

                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(this, "Data Loading Failed", Toast.LENGTH_SHORT).show();
                    break;

                case MachineViewStates.STATE_MACHINE_BUSY:

                    isWebSocketConnected = false;

                    if (trayContentList != null && trayContentList.size() > 0) {
                        mContainer.removeAllViews();
                        setUpRecyclerView();
                    }
                    break;

                case MachineViewStates.STATE_MACHINE_FREE:

                    isWebSocketConnected = true;

                    if (trayContentList != null && trayContentList.size() > 0) {
                        mContainer.removeAllViews();
                        setUpRecyclerView();
                    }
                    break;

                case MachineViewStates.REQUEST_JSON_OBJECT_TO_INITIALISE_DEVICE:

                    machineViewModel.doInitializeEzeTap();
                    break;

                case MachineViewStates.REQUEST_INITIALISE_DEVICE:

                    JSONObject jsonInit = (JSONObject) messageObject.object;
                    EzeAPI.initialize(this, MPOSViewStates.REQUEST_CODE_INITIALIZE, jsonInit);

                    break;

                case MachineViewStates.REQUEST_START_SALE:

                    JSONObject jsonSale = (JSONObject) messageObject.object;
                    EzeAPI.cardTransaction(this, MPOSViewStates.REQUEST_CODE_SALE_TXN, jsonSale);
                    break;
            }

        });
    }

    private void observeDataChange() {

        machineViewModel.getTrayDetailsMutableLiveData().observe(this, trayDetails -> {

            swipeRefreshLayout.setRefreshing(false);
            trayContentList = machineViewModel.getTraysSorted(trayDetails.getTrays());
            maxTrayCount = machineViewModel.getMaxTrayCount(trayContentList);

            setUpRecyclerView();
        });
    }

    private void setUpRecyclerView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < trayContentList.size(); i++) {
            View view = inflater.inflate(R.layout.door_row, mContainer, false);
            RecyclerView recyclerView = view.findViewById(R.id.recycler_machine);
            LinearLayoutManager manager = new LinearLayoutManager(this);
            manager.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerView.setLayoutManager(manager);

            MachineViewProductAdapter machineViewProductAdapter =
                    new MachineViewProductAdapter(this, trayContentList.get(i), maxTrayCount, isWebSocketConnected);
            recyclerView.setAdapter(machineViewProductAdapter);
            recyclerView.addOnScrollListener(scrollListener);
            TextView trayText = view.findViewById(R.id.tray_name);
            trayText.setText("Tray " + trayContentList.get(i).get(0).getTrayName().substring(0, 1));
            view.setId(i);

            observeAdapterDataChange(machineViewProductAdapter);

            mContainer.addView(view);
        }
    }

    private void observeAdapterDataChange(MachineViewProductAdapter machineViewProductAdapter) {

        machineViewProductAdapter.getMachineTraySingleLiveEvent().observeSingle(this,
                this::showPurchaseDialogMsg);
    }

    RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(final @NonNull RecyclerView recyclerView, final int dx, final int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (!isScrolling) {
                isScrolling = true;
                for (int i = 0; i < mContainer.getChildCount(); i++) {
                    final RecyclerView r = mContainer.getChildAt(i).findViewById(R.id.recycler_machine);
                    if (!recyclerView.equals(r))
                        r.scrollBy(dx, dy);
                }
                isScrolling = false;
            }
        }
    };

    private void showPurchaseDialogMsg(MachineTray machineTray) {

        machineTrayValue = machineTray;

        Disposable d = dialogUtils.displaySingleOptionDialog(this,
                String.format(getResources().getString(R.string.dialog_msg_for_opening_door),
                        machineTray.getMachineProduct().getMasterPrice()),
                getResources().getString(R.string.dialog_confirmation))
                .subscribe(aBoolean -> prepareMposDevice());

    }

    private void prepareMposDevice() {

        machineViewModel.doSaleTransaction(machineTrayValue.getMachineProduct().getMasterPrice());
    }

    private void doPrepareDeviceEzeTap() {
        EzeAPI.prepareDevice(this, MPOSViewStates.REQUEST_CODE_PREPARE);
    }

    private void openDoorForTransaction() {

        machineViewModel.openDoor(machineTrayValue);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        try {
            if (intent != null && intent.hasExtra("response")) {
                Timber.d(intent.getStringExtra("response"));
            }

                switch (requestCode) {
                    case MPOSViewStates.REQUEST_CODE_SALE_TXN:

                        if (resultCode == RESULT_OK) {
                            JSONObject response = new JSONObject(intent.getStringExtra("response"));
                            response = response.getJSONObject("result");
                            response = response.getJSONObject("txn");
                            strTxnId = response.getString("txnId");

                            Timber.d("Card transaction successful");

                            openDoorForTransaction();
                        } else if (resultCode == RESULT_CANCELED) {

                            machineViewModel.handleMPosResponse(intent.getStringExtra("response"),
                                    MPOSViewStates.RESPONSE_CODE_SALE_ERROR);

                        }

                        break;
                    case MPOSViewStates.REQUEST_CODE_PREPARE:
                        if (resultCode == RESULT_OK) {

                            Timber.d("Device prepare started");
                            machineViewModel.doSaleTransaction(machineTrayValue.getMachineProduct().getMasterPrice());
                        } else if (resultCode == RESULT_CANCELED) {

                            machineViewModel.handleMPosResponse(intent.getStringExtra("response"),
                                    MPOSViewStates.RESPONSE_CODE_PREPARE_ERROR);
                        }
                        break;

                    case MPOSViewStates.REQUEST_CODE_INITIALIZE:

                        doPrepareDeviceEzeTap();

                        break;

                    case STATE_MACHINE_LIST_SELECTED:
                        if (resultCode == RESULT_OK) {
                            if (intent.getBooleanExtra(MachineViewStates.STATE_MACHINE_LIST_SELECTED, false)) {

                                refreshProductList();
                            }
                        }
                        break;
                    default:
                        break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void refreshProductList() {

        swipeRefreshLayout.setRefreshing(true);
        mContainer.removeAllViews();
        machineViewModel.initialise();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.machine_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_machine_list) {

            Intent intent = new Intent(MachineViewActivity.this, MachineListActivity.class);
            startActivityForResult(intent, STATE_MACHINE_LIST_SELECTED);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        machineViewModel.disposeDisposable();
    }
}
