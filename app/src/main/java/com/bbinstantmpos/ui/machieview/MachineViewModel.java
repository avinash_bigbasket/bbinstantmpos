package com.bbinstantmpos.ui.machieview;

import android.app.Application;

import com.bbinstantmpos.data.source.BBInstantRepository;
import com.bbinstantmpos.entities.MachineTray;
import com.bbinstantmpos.entities.MessageObject;
import com.bbinstantmpos.entities.TrayDetails;
import com.bbinstantmpos.exceptions.MachineBusyException;
import com.bbinstantmpos.exceptions.UnexpectedMessageException;
import com.bbinstantmpos.services.MessengerConstants;
import com.bbinstantmpos.services.WebSocketCommunicator;
import com.bbinstantmpos.util.Preferences;
import com.bbinstantmpos.util.SingleLiveEvent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class MachineViewModel extends AndroidViewModel {

    private BBInstantRepository bbInstantRepository;

    private WebSocketCommunicator webSocketCommunicator;

    private MutableLiveData<TrayDetails> trayDetailsMutableLiveData = new MutableLiveData<>();

    private SingleLiveEvent<MessageObject> onMessageReceived = new SingleLiveEvent<>();

    private CompositeDisposable compositeDisposable;

    private Application mApplication;


    public MachineViewModel(@NonNull Application application, BBInstantRepository bbInstantRepository,
                            WebSocketCommunicator webSocketCommunicator) {
        super(application);

        Timber.plant(new Timber.DebugTree());
        compositeDisposable = new CompositeDisposable();

        this.bbInstantRepository = bbInstantRepository;
        this.webSocketCommunicator = webSocketCommunicator;
        mApplication = application;
    }

    void initialise() {


        fetchProducts();

        connectToWebSocket(webSocketCommunicator.connectToMachine());

        Disposable d = webSocketCommunicator.subscribeToWebSocketMessages()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    switch (s) {

                        case MessengerConstants.MACHINE_DOOR_OPEN:

                            break;

                        case MessengerConstants.MACHINE_DOOR_CLOSED:

                            break;

                        case MessengerConstants.MACHINE_BUSY:

                            onMessageReceived.setValue(new MessageObject(MachineViewStates.STATE_MACHINE_BUSY));
                            break;

                        case MessengerConstants.MACHINE_FREE:

                            onMessageReceived.setValue(new MessageObject(MachineViewStates.STATE_MACHINE_FREE));
                            break;
                    }
                });

        compositeDisposable.add(d);

    }


    private void connectToWebSocket(Observable<Boolean> booleanObservable) {

        Disposable d = booleanObservable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {

                    if (aBoolean) {

                        Timber.d("WebSocket connection successful");
                        onMessageReceived.setValue(new MessageObject(MachineViewStates.STATE_WEB_SOCKET_CONNECTION_SUCCESSFUL));
                    } else {

                        Timber.d("WebSocket connection failed");
                        onMessageReceived.setValue(new MessageObject(MachineViewStates.STATE_WEB_SOCKET_CONNECTION_FAILED));
                    }
                }, throwable -> {

                    Timber.d("WebSocket connection error: " + throwable.getLocalizedMessage());
                    onMessageReceived.setValue(new MessageObject(MachineViewStates.STATE_WEB_SOCKET_CONNECTION_FAILED));

                });

        compositeDisposable.add(d);
    }

    private void fetchProducts() {

        String machineId = Preferences.getPreferences(mApplication.getBaseContext(), Preferences.MACHINE_ID,
                "U5eRHoiPSENB9xIxbddOLb");

        Disposable d = bbInstantRepository.getProductList(machineId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(trayDetails -> trayDetailsMutableLiveData.setValue(trayDetails),
                        throwable -> onMessageReceived.setValue(new MessageObject(MachineViewStates.STATE_ERROR_RECEIVED)));

        compositeDisposable.add(d);
    }

    void openDoor(MachineTray machineTray) {

        Disposable d = webSocketCommunicator.openDoor(machineTray)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                    if (aBoolean) {

                        onMessageReceived.setValue(new MessageObject(MachineViewStates.STATE_DOOR_OPENED));
                    }
                }, throwable -> {
                    if (throwable instanceof MachineBusyException) {

                        onMessageReceived.setValue(new MessageObject(MachineViewStates.STATE_MACHINE_BUSY));
                    } else if (throwable instanceof UnexpectedMessageException) {

                    }
                });

        compositeDisposable.add(d);
    }

    void handleMPosResponse(String res, String msg) {

        try {

            switch (msg) {

                case MPOSViewStates.RESPONSE_CODE_SALE_ERROR:
                case MPOSViewStates.RESPONSE_CODE_PREPARE_ERROR:

                    JSONObject response = new JSONObject(res);
                    response = response.getJSONObject("error");
                    String errorCode = response.getString("code");

                    String errorMessage = response.getString("message");

                    Timber.d("Card transaction failed :: error message :: " + errorMessage);
                    if (errorCode.equals(MPOSViewStates.RESPONSE_CODE_INITIALISATION_ERROR)) {

                        onMessageReceived.setValue(new MessageObject(MachineViewStates.REQUEST_JSON_OBJECT_TO_INITIALISE_DEVICE));
                    }
                    break;


            }

        } catch (Exception e) {
            Timber.d(e.getLocalizedMessage());
        }
    }

    MutableLiveData<TrayDetails> getTrayDetailsMutableLiveData() {

        return trayDetailsMutableLiveData;
    }

    SingleLiveEvent<MessageObject> getOnMessageReceived() {

        return onMessageReceived;
    }

    List<List<MachineTray>> getTraysSorted(List<MachineTray> machineTrays) {

        Map<String, List<MachineTray>> trayMap = new TreeMap<>();
        for (MachineTray tray : machineTrays) {
            String containerName = tray.getTrayName().substring(0, 1);
            if (!trayMap.containsKey(containerName)) {
                List<MachineTray> trays = new ArrayList<>();
                trays.add(tray);
                trayMap.put(containerName, trays);
            } else {
                trayMap.get(containerName).add(tray);
            }
        }

        List<List<MachineTray>> sortedMachineTrays = new ArrayList<>();
        for (Map.Entry<String, List<MachineTray>> entry : trayMap.entrySet()) {
            Collections.sort(entry.getValue(), new Comparator<MachineTray>() {
                @Override
                public int compare(MachineTray o1, MachineTray o2) {
                    return o1.getTrayName().compareTo(o2.getTrayName());
                }
            });
            sortedMachineTrays.add(entry.getValue());
        }

        return sortedMachineTrays;
    }

    int getMaxTrayCount(List<List<MachineTray>> trayContentList) {

        int maxTrayCount = 0;
        for (List<MachineTray> trayList : trayContentList) {
            if (trayList.size() > maxTrayCount) {

                maxTrayCount = trayList.size();
            }
        }

        return maxTrayCount;
    }


    void doInitializeEzeTap() {

        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("demoAppKey", "d33e4a14-406b-4b64-9e33-db2dabfdef82"/*Setting.config.getAppKey()*/);
            jsonRequest.put("prodAppKey", "d33e4a14-406b-4b64-9e33-db2dabfdef82"/*Setting.config.getAppKey()*/);
            jsonRequest.put("merchantName", "BBInstant");
            jsonRequest.put("userName", "BB Instant");
            jsonRequest.put("currencyCode", "INR");
            jsonRequest.put("appMode", "DEMO");
            jsonRequest.put("captureSignature", "true");
            jsonRequest.put("prepareDevice", "false");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        onMessageReceived.setValue(new MessageObject(MachineViewStates.REQUEST_INITIALISE_DEVICE, jsonRequest));
    }

    void doSaleTransaction(String amount) {

        long currentTime = System.currentTimeMillis();
        String orderNumber = "BB" + (currentTime % 100000);
        JSONObject jsonRequest = new JSONObject();
        try {

            JSONObject jsonOptions = new JSONObject();
            JSONObject jsonReferences = new JSONObject();


            jsonReferences.put("reference1", String.valueOf(orderNumber));
            jsonOptions.put("references", jsonReferences);

            jsonRequest.put("amount", amount);
            jsonRequest.put("options", jsonOptions);
            jsonRequest.put("mode", "SALE");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        onMessageReceived.setValue(new MessageObject(MachineViewStates.REQUEST_START_SALE, jsonRequest));
    }

    void disposeDisposable() {

        compositeDisposable.dispose();
    }

}
