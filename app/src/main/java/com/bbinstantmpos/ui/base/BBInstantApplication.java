package com.bbinstantmpos.ui.base;

import com.bbinstantmpos.di.component.BBInstantComponent;
import com.bbinstantmpos.di.component.DaggerBBInstantComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class BBInstantApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        BBInstantComponent bbInstantComponent =
                DaggerBBInstantComponent.builder().applicaton(this).build();
        bbInstantComponent.inject(this);
        return bbInstantComponent;
    }
}
