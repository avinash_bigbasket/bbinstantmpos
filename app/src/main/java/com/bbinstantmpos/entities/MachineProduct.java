package com.bbinstantmpos.entities;

import com.google.gson.annotations.SerializedName;

public class MachineProduct {

    @SerializedName("productid")
    private String productid;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    @SerializedName("category")
    private String categoty;

    @SerializedName("price")
    private String qty;

    @SerializedName("weight")
    private String weight;

    @SerializedName("ingridents")
    private String ingridents;

    @SerializedName("discountPercentage")
    private int discountPercentage;

    @SerializedName("isWeightInconsistent")
    private boolean isWeightInconsistent;

    @SerializedName("masterPrice")
    private String masterPrice;

    @SerializedName("measurement")
    private String measurement;

    @SerializedName("dietaryType")
    private String dietaryType;


    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategoty() {
        return categoty;
    }

    public void setCategoty(String categoty) {
        this.categoty = categoty;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getIngridents() {
        return ingridents;
    }

    public void setIngridents(String ingridents) {
        this.ingridents = ingridents;
    }

    public int getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(int discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public boolean isWeightInconsistent() {
        return isWeightInconsistent;
    }

    public void setWeightInconsistent(boolean weightInconsistent) {
        isWeightInconsistent = weightInconsistent;
    }

    public String getMasterPrice() {
        return masterPrice;
    }

    public void setMasterPrice(String masterPrice) {
        this.masterPrice = masterPrice;
    }

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public String getDietaryType() {
        return dietaryType;
    }

    public void setDietaryType(String dietaryType) {
        this.dietaryType = dietaryType;
    }
}
