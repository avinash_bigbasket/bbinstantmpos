package com.bbinstantmpos.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;


public class Locations implements Serializable {

    @SerializedName("id")
    private String locationId;
    @SerializedName("name")
    private String locationName;
    @SerializedName("country")
    private String country;
    @SerializedName("city")
    private String city;
    @SerializedName("street")
    private String street;
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("longitude")
    private String longitude;
    @SerializedName("distance")
    private String distance;
    @SerializedName("pin")
    private String pin;
    @SerializedName("machines")
    private ArrayList<Machines> machines;

    public Locations(String locationId, String locationName, String country, String city,
                     String street, String latitude, String longitude, String distance,
                     String pin, ArrayList<Machines> machines) {
        this.locationId = locationId;
        this.locationName = locationName;
        this.country = country;
        this.city = city;
        this.street = street;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
        this.pin = pin;
        this.machines = machines;
    }

    public Locations(String locationId, String locationName, String country, String city,
                     String street, String latitude, String longitude, String distance,
                     String pin) {
        this.locationId = locationId;
        this.locationName = locationName;
        this.country = country;
        this.city = city;
        this.street = street;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
        this.pin = pin;

    }

    public Locations(String locationId) {
        this.locationId = locationId;
    }

    public static class OrderByLocationName implements Comparator<Locations> {

        @Override
        public int compare(Locations loc1, Locations loc2) {
            return loc1.locationName.compareTo(loc2.locationName);
        }
    }



    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public ArrayList<Machines> getMachines() {
        return machines;
    }

    public void setMachines(ArrayList<Machines> machines) {
        this.machines = machines;
    }

    @Override
    public boolean equals(Object object){
        return locationId.equals(((Locations)object).locationId);
    }


    public boolean deepEquals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Locations locations = (Locations) o;

        if (locationId != null ? !locationId.equals(locations.locationId) : locations.locationId != null)
            return false;
        if (locationName != null ? !locationName.equals(locations.locationName) : locations.locationName != null)
            return false;
        if (country != null ? !country.equals(locations.country) : locations.country != null)
            return false;
        if (city != null ? !city.equals(locations.city) : locations.city != null) return false;
        if (street != null ? !street.equals(locations.street) : locations.street != null)
            return false;
        if (latitude != null ? !latitude.equals(locations.latitude) : locations.latitude != null)
            return false;
        if (longitude != null ? !longitude.equals(locations.longitude) : locations.longitude != null)
            return false;
        if (distance != null ? !distance.equals(locations.distance) : locations.distance != null)
            return false;
        if (pin != null ? !pin.equals(locations.pin) : locations.pin != null) return false;
        return machines != null ? deepCheckAllMachines(machines, locations.machines) : locations.machines == null;
    }

    private boolean deepCheckAllMachines(ArrayList<Machines> machines, ArrayList<Machines> machines1) {

        if (!machines.getClass().equals(machines1.getClass())) return false;
        else if (machines.size()!=machines1.size()) return false;
        else{
            for (int i=0;i<machines.size();i++){
                boolean machineMarker = false;
                Machines iMachine = machines.get(i);
                for (int j=0;j<machines1.size();j++){
                    Machines jMachine = machines1.get(j);
                    if (iMachine.getMachineId().equals(jMachine.getMachineId())){
                        machineMarker = iMachine.deepEquals(jMachine);
                    }
                }
                if (!machineMarker) return false;
            }
            return true;
        }
    }

    @Override
    public String toString() {

       return
                this.getLocationId() + " , " +
                this.getLocationName() + " , " +
                this.getCountry() + " , " +
                this.getCity() + " , " +
                this.getStreet() + " , " +
                this.getLatitude() + " , " +
                this.getLongitude() + " , " +
                this.getDistance() + " , " +
                this.getPin() ;


    }
}
