package com.bbinstantmpos.entities;

import com.google.gson.annotations.SerializedName;

public class MachineTray {
    @SerializedName("id")
    String trayId;

    @SerializedName("name")
    String trayName;

    @SerializedName("containerId")
    String containerId;

    @SerializedName("lockcode")
    String lockCode;

    @SerializedName("quantity")
    int qtyOnTray;

    @SerializedName("containerStatus")
    String containerStatus;

    @SerializedName("productSource")
    String productSource;

    @SerializedName("product")
    MachineProduct machineProduct;

    public String getTrayId() {
        return trayId;
    }

    public void setTrayId(String trayId) {
        this.trayId = trayId;
    }

    public String getTrayName() {
        return trayName;
    }

    public void setTrayName(String trayName) {
        this.trayName = trayName;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getLockCode() {
        return lockCode;
    }

    public void setLockCode(String lockCode) {
        this.lockCode = lockCode;
    }

    public int getQtyOnTray() {
        return qtyOnTray;
    }

    public void setQtyOnTray(int qtyOnTray) {
        this.qtyOnTray = qtyOnTray;
    }

    public String getContainerStatus() {
        return containerStatus;
    }

    public void setContainerStatus(String containerStatus) {
        this.containerStatus = containerStatus;
    }

    public String getProductSource() {
        return productSource;
    }

    public void setProductSource(String productSource) {
        this.productSource = productSource;
    }

    public MachineProduct getMachineProduct() {
        return machineProduct;
    }

    public void setMachineProduct(MachineProduct machineProduct) {
        this.machineProduct = machineProduct;
    }
}
