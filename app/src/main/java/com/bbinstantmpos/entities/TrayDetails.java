package com.bbinstantmpos.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TrayDetails {
    @SerializedName("count")
    private int count;

    @SerializedName("trays")
    List<MachineTray> trays;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<MachineTray> getTrays() {
        return trays;
    }

    public void setTrays(List<MachineTray> trays) {
        this.trays = trays;
    }
}
