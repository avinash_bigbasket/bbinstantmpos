package com.bbinstantmpos.entities;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Machines implements Serializable {
    public static final String MQTT_PROTOCOL = "mqtt";
    public static final String WS_PROTOCOL = "ws";

    @SerializedName("id")
    private String machineId;
    @SerializedName("name")
    private String machineName;
    @SerializedName("state")
    private String machineState;
    @SerializedName("container")
    private String machineContainer;
    @SerializedName("beacon_id")
    private String machineBeaconId;
    @SerializedName("beacon_major")
    private String machineBeaconMajor;
    @SerializedName("beacon_minor")
    private String machineBeaconMinor;
    @SerializedName("wifissid")
    private String wifissid;
    @SerializedName("nearableidentifier")
    private String nearableidentifier;
    @SerializedName("payment_type")
    private String paymentType;
    @SerializedName("wifirssi")
    private int wifirssi;
    @SerializedName("wifiName")
    private String wifiName;
    @SerializedName("wifiPassword")
    private String wifiPassword;
    @SerializedName("wifiSecurityType")
    private String wifiSecurityType;
    @SerializedName("offlineIpAddress")
    private String offlineIpAddress;
    @SerializedName("offlinePort")
    private String offlinePort;
    @SerializedName("offlineEnabled")
    private String offlineEnabled;
    @SerializedName("location_id")
    private String location_id;
    @SerializedName("type")
    private String type;
    @SerializedName("protocol")
    private String protocol;
    @SerializedName("localwsEnabled")
    private int  localwsEnabled;
    @SerializedName("displayName")
    private String displayName;

    private String locationName;

    public Machines() {
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public String getMachineState() {
        return machineState;
    }

    public void setMachineState(String machineState) {
        this.machineState = machineState;
    }

    public String getMachineContainer() {
        return machineContainer;
    }

    public void setMachineContainer(String machineContainer) {
        this.machineContainer = machineContainer;
    }

    public String getMachineBeaconId() {
        return machineBeaconId;
    }

    public void setMachineBeaconId(String machineBeaconId) {
        this.machineBeaconId = machineBeaconId;
    }

    public String getMachineBeaconMajor() {
        return machineBeaconMajor;
    }

    public void setMachineBeaconMajor(String machineBeaconMajor) {
        this.machineBeaconMajor = machineBeaconMajor;
    }

    public String getMachineBeaconMinor() {
        return machineBeaconMinor;
    }

    public void setMachineBeaconMinor(String machineBeaconMinor) {
        this.machineBeaconMinor = machineBeaconMinor;
    }

    public String getWifissid() {
        return wifissid;
    }

    public void setWifissid(String wifissid) {
        this.wifissid = wifissid;
    }

    public String getNearableidentifier() {
        return nearableidentifier;
    }

    public void setNearableidentifier(String nearableidentifier) {
        this.nearableidentifier = nearableidentifier;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }


    public int getWifirssi() {
        return wifirssi;
    }

    public void setWifirssi(int wifirssi) {
        this.wifirssi = wifirssi;
    }

    public String getWifiName() {
        return wifiName;
    }

    public void setWifiName(String wifiName) {
        this.wifiName = wifiName;
    }

    public String getWifiPassword() {
        return wifiPassword;
    }

    public void setWifiPassword(String wifiPassword) {
        this.wifiPassword = wifiPassword;
    }

    public String getWifiSecurityType() {
        return wifiSecurityType;
    }

    public void setWifiSecurityType(String wifiSecurityType) {
        this.wifiSecurityType = wifiSecurityType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOfflineIpAddress() {
        return offlineIpAddress;
    }

    public void setOfflineIpAddress(String offlineIpAddress) {
        this.offlineIpAddress = offlineIpAddress;
    }

    public String getOfflinePort() {
        return offlinePort;
    }

    public void setOfflinePort(String offlinePort) {
        this.offlinePort = offlinePort;
    }

    public String getOfflineEnabled() {
        return offlineEnabled;
    }

    public void setOfflineEnabled(String offlineEnabled) {
        this.offlineEnabled = offlineEnabled;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public boolean isMqtt() {
        if (protocol == null || TextUtils.isEmpty(protocol)) return false;
        return protocol.equals(MQTT_PROTOCOL);
    }

    public int getLocalwsEnabled() {
        return localwsEnabled;
    }

    public void setLocalwsEnabled(int localwsEnabled) {
        this.localwsEnabled = localwsEnabled;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public Machines(String machineId, String machineName, String machineState, String machineContainer, String machineBeaconId, String machineBeaconMajor, String machineBeaconMinor, String wifissid, String nearableidentifier, String paymentType, int wifirssi, String wifiName, String wifiPassword, String wifiSecurityType, String offlineIpAddress, String offlinePort, String offlineEnabled, String location_id, String type, String protocol, int localwsEnabled) {
        this.machineId = machineId;
        this.machineName = machineName;
        this.machineState = machineState;
        this.machineContainer = machineContainer;
        this.machineBeaconId = machineBeaconId;
        this.machineBeaconMajor = machineBeaconMajor;
        this.machineBeaconMinor = machineBeaconMinor;
        this.wifissid = wifissid;
        this.nearableidentifier = nearableidentifier;
        this.paymentType = paymentType;
        this.wifirssi = wifirssi;
        this.wifiName = wifiName;
        this.wifiPassword = wifiPassword;
        this.wifiSecurityType = wifiSecurityType;
        this.offlineIpAddress = offlineIpAddress;
        this.offlinePort = offlinePort;
        this.offlineEnabled = offlineEnabled;
        this.location_id = location_id;
        this.type = type;
        this.protocol = protocol;
        this.localwsEnabled = localwsEnabled;
    }

    @Override
    public String toString() {
        return "Machines{" +
                "machineId='" + machineId + '\'' +
                ", machineName='" + machineName + '\'' +
                ", machineState='" + machineState + '\'' +
                ", machineContainer='" + machineContainer + '\'' +
                ", machineBeaconId='" + machineBeaconId + '\'' +
                ", machineBeaconMajor='" + machineBeaconMajor + '\'' +
                ", machineBeaconMinor='" + machineBeaconMinor + '\'' +
                ", wifissid='" + wifissid + '\'' +
                ", nearableidentifier='" + nearableidentifier + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", wifirssi=" + wifirssi +
                ", wifiName='" + wifiName + '\'' +
                ", wifiPassword='" + wifiPassword + '\'' +
                ", wifiSecurityType='" + wifiSecurityType + '\'' +
                ", offlineIpAddress='" + offlineIpAddress + '\'' +
                ", offlinePort='" + offlinePort + '\'' +
                ", offlineEnabled='" + offlineEnabled + '\'' +
                ", protocol='" + protocol + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        String machineId = ((Machines) obj).getMachineId();

        return machineId.equals(this.machineId);
    }

    public boolean deepEquals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Machines machines = (Machines) o;

        if (wifirssi != machines.wifirssi) return false;
        if (machineId != null ? !machineId.equals(machines.machineId) : machines.machineId != null)
            return false;
        if (machineName != null ? !machineName.equals(machines.machineName) : machines.machineName != null)
            return false;
        if (machineState != null ? !machineState.equals(machines.machineState) : machines.machineState != null)
            return false;
        if (machineContainer != null ? !machineContainer.equals(machines.machineContainer) : machines.machineContainer != null)
            return false;
        if (machineBeaconId != null ? !machineBeaconId.equals(machines.machineBeaconId) : machines.machineBeaconId != null)
            return false;
        if (machineBeaconMajor != null ? !machineBeaconMajor.equals(machines.machineBeaconMajor) : machines.machineBeaconMajor != null)
            return false;
        if (machineBeaconMinor != null ? !machineBeaconMinor.equals(machines.machineBeaconMinor) : machines.machineBeaconMinor != null)
            return false;
        if (wifissid != null ? !wifissid.equals(machines.wifissid) : machines.wifissid != null)
            return false;
        if (nearableidentifier != null ? !nearableidentifier.equals(machines.nearableidentifier) : machines.nearableidentifier != null)
            return false;
        if (paymentType != null ? !paymentType.equals(machines.paymentType) : machines.paymentType != null)
            return false;
        if (wifiName != null ? !wifiName.equals(machines.wifiName) : machines.wifiName != null)
            return false;
        if (wifiPassword != null ? !wifiPassword.equals(machines.wifiPassword) : machines.wifiPassword != null)
            return false;
        if (wifiSecurityType != null ? !wifiSecurityType.equals(machines.wifiSecurityType) : machines.wifiSecurityType != null)
            return false;
        if (offlineIpAddress != null ? !offlineIpAddress.equals(machines.offlineIpAddress) : machines.offlineIpAddress != null)
            return false;
        if (offlinePort != null ? !offlinePort.equals(machines.offlinePort) : machines.offlinePort != null)
            return false;
        if (offlineEnabled != null ? !offlineEnabled.equals(machines.offlineEnabled) : machines.offlineEnabled != null)
            return false;
        return protocol != null ? protocol.equals(machines.protocol) : machines.protocol == null;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
