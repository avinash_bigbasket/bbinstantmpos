package com.bbinstantmpos.entities;

public class MessageObject {

    public String message;
    public Object object;

    public MessageObject(String message, Object object) {

        this.message = message;
        this.object = object;
    }

    public MessageObject(String message) {
        this(message, null);
    }
}
