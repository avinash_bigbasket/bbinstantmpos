package com.bbinstantmpos.data.source;

import com.bbinstantmpos.entities.Locations;
import com.bbinstantmpos.entities.TrayDetails;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class BBInstantRepository {

    private BBInstantService bbInstantService;

    @Inject
    public BBInstantRepository(BBInstantService bbInstantServices) {

        this.bbInstantService = bbInstantServices;
    }

    public Single<TrayDetails> getProductList(String machineID) {

        return bbInstantService.getProducts(machineID);
    }

    public Single<List<Locations>> getLocationList() {

        return bbInstantService.getLocations();
    }

}
