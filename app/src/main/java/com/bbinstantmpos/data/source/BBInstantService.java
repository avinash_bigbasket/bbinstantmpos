package com.bbinstantmpos.data.source;

import com.bbinstantmpos.entities.Locations;
import com.bbinstantmpos.entities.TrayDetails;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface BBInstantService {

    @GET("kwik24/v2/machine/{machineID}/trays?details=true")
    Single<TrayDetails> getProducts(@Path("machineID") String machineId);

    @POST("auth/token")
    @FormUrlEncoded
    void refreshToken(
            @Field("username") String username,
            @Field("refreshToken") String refreshToken
    );

    @GET("kwik24/v3/location?active=true")
    Single<List<Locations>> getLocations();
}
