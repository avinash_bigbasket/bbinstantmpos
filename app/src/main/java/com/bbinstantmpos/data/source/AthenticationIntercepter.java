package com.bbinstantmpos.data.source;

import android.content.Context;
import android.content.res.Resources;


import com.bbinstantmpos.R;
import com.bbinstantmpos.util.Preferences;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AthenticationIntercepter implements Interceptor {

    Resources resources;
    Context context;

    public AthenticationIntercepter(Context context) {

        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        HttpUrl path = originalRequest.url();

        String athenticationToken = null;
        if (!isTokenUrl(path.toString())){
            athenticationToken = context.getResources().getString(R.string.long_live_token);
            athenticationToken = String.format("Bearer %s", athenticationToken);
            Request.Builder builder = originalRequest.newBuilder();
            builder.addHeader("Authorization", athenticationToken);
            return chain.proceed(builder.build());
        }

        else return chain.proceed(originalRequest);
    }

    public boolean isCreateUserUrl(String s) {
        return s.endsWith("auth/v2/users");
    }

    public boolean isTokenUrl(String s) {
        return s.contains("auth/token");
    }
}
