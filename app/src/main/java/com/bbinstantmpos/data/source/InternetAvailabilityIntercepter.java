package com.bbinstantmpos.data.source;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class InternetAvailabilityIntercepter implements Interceptor {


    public InternetAvailabilityIntercepter() {

    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        return chain.proceed(chain.request());
    }
}
