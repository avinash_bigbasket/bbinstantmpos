package com.bbinstantmpos.data.source;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;

import com.bbinstantmpos.util.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import okhttp3.Authenticator;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;

public class TokenAuthenticator implements Authenticator {

    String clientId;
    String clientSecret;

    Context context;

    String host = "http://stage.api.kwik24.com:8080/";
    String host_old = "http://stage.api.kwik24.com:8080/";
    private static final String URL_REGEX = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

    public TokenAuthenticator(Context context) {

        this.context = context;
    }

    @Override
    public Request authenticate(Route route, Response response) {
            try {
                String previousAuthorization = response.request().header("Authorization");
                if (previousAuthorization != null
                        && previousAuthorization.startsWith("Basic")) {
                    return null; // Give up, we've already failed to authenticate.
                }
                String accessToken = synchronousAuthTokenRefresh();
                return response.request().newBuilder()
                        .header("Authorization", String.format("Bearer %s", accessToken))
                        .build();
            } catch (Exception e) {
                return null;
            }
    }

    @NonNull
    private String synchronousAuthTokenRefresh() throws IOException, JSONException {
        String baseToken = convertToBase4(clientId
                + ":" + clientSecret);
        if (TextUtils.isEmpty(clientSecret)
                || TextUtils.isEmpty(clientId)) {
            throw new IllegalArgumentException("User not created yet!!");
        }
        String athenticationToken = String.format("Basic %s", baseToken);
        RequestBody requestBody = new FormBody.Builder()
                .add("grant_type", "client_credentials")
                .add("scope", "default")
                .build();
        Request request = new Request.Builder()
                .url(provideAuthUrl())
                .addHeader("Authorization", athenticationToken)
                .post(requestBody)
                .build();
        try (Response response = new OkHttpClient().newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("unexpected code " + response);
            String accessTokenInBody = response.body().string();
            JSONObject json = new JSONObject(accessTokenInBody);
            if (json != null && json.has("accessToken")) {

                String accessToken = json.getString("accessToken");

                Preferences.setPreferences(context, "accessToken", accessToken);
                return json.getString("accessToken");
            } else throw new IOException("no access token found");
        }
    }

    public String convertToBase4(String string) {
        byte[] encodeValue = Base64.encode(string.getBytes(), Base64.NO_WRAP);
        return new String(encodeValue);
    }

    public String provideAuthUrl() {

        String authUrlBuilder = provideBaseUrl() +
                "auth/token";
        return authUrlBuilder;
    }

    public String provideBaseUrl(){
        String baseUrl = null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            baseUrl = host_old;
        else baseUrl = host;
        return baseUrl;
    }

    public boolean isValidUrl(String url){
        try {
            Pattern patt = Pattern.compile(URL_REGEX);
            Matcher matcher = patt.matcher(url);
            return matcher.matches();
        } catch (RuntimeException e) {
            return false;
        }
    }
}