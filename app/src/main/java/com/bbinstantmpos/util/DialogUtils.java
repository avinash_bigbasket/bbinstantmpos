package com.bbinstantmpos.util;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;


import com.bbinstantmpos.R;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class DialogUtils {

    @Inject
    public DialogUtils() {

    }

    public Observable<String> displayDialog(Context context, String msg, String positive,
                                            String negative) {
        return Observable.fromPublisher(s -> {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.DialogTheme)
                    .setCancelable(true)
                    .setMessage(msg);
            if (isValidOption(positive))
                dialogBuilder.setPositiveButton(positive, (dialog1, which) -> {
                    s.onNext(positive);
                    s.onComplete();
                    dialog1.dismiss();
                });
            if (isValidOption(negative))
                dialogBuilder.setNegativeButton(negative, (dialog1, which) -> {
                    s.onNext(negative);
                    s.onComplete();
                    dialog1.dismiss();
                });
            dialogBuilder.create().show();
        });
    }

    public Observable<Boolean> displaySingleOptionDialog(Context context, String msg, String option) {
        return displayDialog(context, msg, option, null)
                .map(s -> true);
    }

    private boolean isValidOption(String positive) {
        return positive != null && !positive.isEmpty();
    }

}
