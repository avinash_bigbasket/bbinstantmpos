package com.bbinstantmpos.util;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

import com.bbinstantmpos.di.module.BaseModule;

public class DeviceUtils extends BaseModule {

    private Context context;

    public DeviceUtils(Context application) {

        this.context = application;
    }

    public int getWidthOfScreen(){
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display defaultDisplay = windowManager.getDefaultDisplay();
        Point size = new Point();
        defaultDisplay.getSize(size);
        return size.x;
    }

    public int getHeightOfScreen(){
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display defaultDisplay = windowManager.getDefaultDisplay();
        Point size = new Point();
        defaultDisplay.getSize(size);
        return size.y;
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}
