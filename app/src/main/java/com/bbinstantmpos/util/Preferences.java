package com.bbinstantmpos.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class Preferences {
    private static final String PREF_TAG = "SHARED_PREFERENCE";

    public static final String OFFLINE_IP = "OFFLINE_IP";
    public static final String OFFLINE_PORT = "OFFLINE_PORT";
    public static final String MACHINE_ID = "MACHINE_ID";
    public static final String CLIENT_ID = "CLIENT_ID";

    public static void setPreferences(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();
    }

    public static void setPreferences(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();
    }

    public static void setPreferences(Context context, String key, Boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public static void setFloatPreferences(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).commit();
    }

    public static String getPreferences(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    public static int getPreferences(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    public static float getFloatPreferences(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    public static Boolean getPreferences(Context context, String key, Boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    public static boolean isContains(Context context, String key) {
        return getPreferences(context).contains(key);
    }

    public static void remove(Context context, String key) {
        getEditor(context).remove(key).commit();
    }

    public static void clealAll(Context context) {
        getEditor(context).clear().commit();
    }

    private static Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_TAG, 0);
    }
}
