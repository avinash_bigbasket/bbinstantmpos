package com.bbinstantmpos.util;

import android.app.Application;

import com.bbinstantmpos.data.source.BBInstantRepository;
import com.bbinstantmpos.services.WebSocketCommunicator;
import com.bbinstantmpos.ui.machieview.MachineViewModel;
import com.bbinstantmpos.ui.machinelist.MachineListViewModel;
//import com.bbinstantmpos.ui.machinelist.MachineListViewModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider.NewInstanceFactory;

public class ViewModelFactory extends NewInstanceFactory {

    private WebSocketCommunicator webSocketCommunicator;
    private Application mApplication;

    @Inject
    BBInstantRepository bbInstantRepository;

    @Inject
    public ViewModelFactory(Application application, WebSocketCommunicator webSocketCommunicator) {

        mApplication = application;
        this.webSocketCommunicator = webSocketCommunicator;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

        if (modelClass.isAssignableFrom(MachineViewModel.class)) {
            return (T) new MachineViewModel(mApplication, bbInstantRepository, webSocketCommunicator);
        } else if (modelClass.isAssignableFrom(MachineListViewModel.class)) {

            return (T) new MachineListViewModel(mApplication, bbInstantRepository);
        }
        return super.create(modelClass);
    }
}
