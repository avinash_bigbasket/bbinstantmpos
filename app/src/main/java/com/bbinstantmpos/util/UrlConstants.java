package com.bbinstantmpos.util;

import android.os.Build;

import com.bbinstantmpos.BuildConfig;

public class UrlConstants {

    public static final String KW_HTTP_PROTOCOL;
    public static final String BUILD_CONFIG_HOST;
    public static final String BASE_URL;

    static {
        if (BuildConfig.FLAVOR.equals("prod")) {

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                BUILD_CONFIG_HOST = BuildConfig.HOST_OLDER_VERSION;
                KW_HTTP_PROTOCOL = "http://";
            } else {
                BUILD_CONFIG_HOST = BuildConfig.HOST;
                KW_HTTP_PROTOCOL = "https://";
            }
        } else {
            BUILD_CONFIG_HOST = BuildConfig.HOST_OLDER_VERSION;
            KW_HTTP_PROTOCOL = "http://";
        }

        BASE_URL = KW_HTTP_PROTOCOL + BUILD_CONFIG_HOST;
    }
}
